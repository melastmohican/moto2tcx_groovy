import java.text.SimpleDateFormat
import groovy.xml.MarkupBuilder

class Moto2Tcx {

	def process(String input) {
		SimpleDateFormat ISO8601UTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
		ISO8601UTC.setTimeZone(TimeZone.getTimeZone("UTC"))

		def csv= new File(input);
		def header = [];
		def lines = [];
		csv.eachLine() { line, count ->
			if (count == 1) {
				header = line.toString().replaceAll(/"/, '').tokenize(',');
			}
			else {
				def items = line.toString().replaceAll(/"/, '').tokenize(',');
				def row = [:];
				items.eachWithIndex { item, index ->
					def key = header[index];
					def val = item;
					row.put(key, val);
				}
				lines << row;
			}
		}
		//println lines.grep({ (new Double(it.LATITUDE) != 0.0  && new Double(it.LONGITUDE) != 0.0) })

		int size = lines.size()
		def firstLine = lines[0]
		def lastLine = lines[size-1]
		def sumHR = lines.sum() { line -> return new Double(line.HEARTRATE).toInteger() }
		def avgHR = new Double(sumHR / lines.size()).toInteger()

		def maxHRLine = lines.max() { line -> return new Double(line.HEARTRATE).toInteger() }
		def maxHR = new Double(maxHRLine.HEARTRATE).toInteger()

		def maxSpeedLine = lines.max() { line -> return new Double(line.SPEED) }
		def maxSpeed = new Double(maxSpeedLine.SPEED)

		long start = new Long(firstLine.timestamp_epoch)
		long end = new Long(lastLine.timestamp_epoch)
		long total = (end-start)/1000
		def distance = lastLine.DISTANCE
		int calories = new Double(lastLine.CALORIEBURN).toInteger()
		def sport = "Other";
		switch (new Integer(firstLine.activity_id)) {
			case 1:
			case 2:
				sport = "Running";
				break;
			case 4:
				sport =  "Biking";
				break
			default:
				sport "Other";
		}

		def writer = new StringWriter()
		def builder = new MarkupBuilder(writer)
		builder.setDoubleQuotes(true)
		builder.mkp.xmlDeclaration(version:"1.0", encoding:"UTF-8", standalone:"no")
		def xml = builder.TrainingCenterDatabase(xmlns:"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2",
		"xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance",
		"xsi:schemaLocation":"http://www.garmin.com/xmlschemas/ActivityExtension/v2 http://www.garmin.com/xmlschemas/ActivityExtensionv2.xsd http://www.garmin.com/xmlschemas/FatCalories/v1 http://www.garmin.com/xmlschemas/fatcalorieextensionv1.xsd http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd") {
			Folders()
			Activities() {
				Activity(Sport:sport) {
					Id(ISO8601UTC.format(new Date(start)))
					Lap(StartTime:ISO8601UTC.format(start)) {
						TotalTimeSeconds(total)
						DistanceMeters(distance)
						MaximumSpeed(maxSpeed)
						Calories(calories)
						AverageHeartRateBpm() { Value(avgHR) }
						MaximumHeartRateBpm() { Value(maxHR) }
						Intensity("Active")
						TriggerMethod("Distance")
						Track() {
							lines.grep({ (new Double(it.LATITUDE) != 0.0  && new Double(it.LONGITUDE) != 0.0) }).each { line ->
								Trackpoint() {
									Time(ISO8601UTC.format(new Date(new Long(line.timestamp_epoch))))
									Position() {
										LatitudeDegrees(line.LATITUDE)
										LongitudeDegrees(line.LONGITUDE)
									}
									AltitudeMeters(line.ELEVATION)
									DistanceMeters(line.DISTANCE)
									HeartRateBpm() {
										Value(new Double(line.HEARTRATE).toInteger())
									}
									Cadence(line.CADENCE)
									SensorState("Absent")
								}
							}

						}
					}
					Creator("xsi:type":"Device_t") {
						Name("melastmohican")
						UnitId("7")
						ProductID("7")
						Version() {
							VersionMajor(1)
							VersionMinor(0)
							BuildMajor(1)
							BuildMinor(0)
						}
					}
				}
			}
			Workouts()
			Courses()
			Author("xsi:type":"Application_t") {
				Name("Garmin Training Center")
				Build() {
					Version() {
						VersionMajor(1)
						VersionMinor(0)
						BuildMajor(1)
						BuildMinor(0)
					}
					Type("Release")
					Time("Jan  1 2012, 22:00:00")
					Builder("melastmohican")
				}
				LangID("en")
				PartNumber("006-A0183-00")
			}

		}
		//println writer.toString()
		def inputFile = new File(input).getName()
		def outputFile = inputFile.lastIndexOf('.').with {it != -1 ? inputFile[0..<it] : inputFile} + ".tcx"
		def tcxFile = new File(outputFile)
		tcxFile.write(writer.toString())
	}

	static void main(args) {

		def mt = new Moto2Tcx()

		def cli = new CliBuilder(usage: 'groovy Moto2Tcx.groovy -f[h] input')
		cli.h(longOpt: 'help'  , 'usage information'   , required: false           )
		cli.f(longOpt: 'file', 'input file', required: true  , args: 1 )
		OptionAccessor opt = cli.parse(args)
		if(!opt) {
			return
		}

		if(opt.h ) {
			cli.usage()
			return
		}
		if( opt.f ) {
			mt.process(opt.f)
		}
	}
}
